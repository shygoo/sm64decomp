#
# mips64-elf-gcc
#  https://cen64.com/uploads/n64chain-win64-tools.zip
#  https://cen64.com/uploads/n64chain-linux64-tools.tgz
#
# armips
#  https://buildbot.orphis.net/armips/
#
# n64crc
#  http://n64dev.org/n64crc.html
#
# 64drive_usb
#  http://64drive.retroactive.be/dl.php?id=usb2xx
#  https://github.com/RenaKunisaki/64drive-usb-linux/
#

CC=mips64-elf-gcc
CFLAGS=-O3 -mtune=vr4300 -march=vr4300 -mabi=32 -fomit-frame-pointer -G0 

AS=armips
CRC=n64crc

ODIR=obj
SDIR=src
TDIR=tmp

ROM_IN=SM64.z64
ROM_OUT=SM64_OUT.z64

ASM_MAIN=main.asm

############

.PHONY: rom
rom: $(ROM_OUT)

############

$(ODIR)/hud.o: $(SDIR)/hud.c $(SDIR)/ci.h | $(ODIR)
	$(CC) $(CFLAGS) -c $(SDIR)/hud.c -o $(ODIR)/hud.o

$(ODIR):
	mkdir $(ODIR)

$(TDIR):
	mkdir $(TDIR)

############

N64_WRAP=$(TDIR)/armips_n64_wrapper.asm

$(N64_WRAP): makefile | $(TDIR)
	echo ".n64" > $(N64_WRAP)
	echo ".open \"$(ROM_IN)\", \"$(ROM_OUT)\", 0" >> $(N64_WRAP)
	echo ".include \"$(ASM_MAIN)\"" >> $(N64_WRAP)
	echo ".close" >> $(N64_WRAP)

############

# just compiling the hud func for now

$(ROM_OUT): $(ODIR)/hud.o $(ROM_IN) $(N64_WRAP) $(ASM_MAIN) | $(ODIR)
	$(AS) $(N64_WRAP) -root .
	$(CRC) $(ROM_OUT)

############
	
.PHONY: clean
clean:
	rm -rf $(ODIR)
	rm -rf $(TDIR)
	rm -f $(ROM_OUT)
	rm -f $(N64_WRAP)

############

.PHONY: view
view: $(ODIR)/hud.o
	mips64-elf-objdump -S -r $(ODIR)/hud.o
	
############

# Build and send rom to 64drive

.PHONY: load
load: $(ROM_OUT)
	64drive_usb -l $(ROM_OUT) -s 1 -c 6102
	

.PHONY: dump
dump:
	64drive_usb -d sm64_dump.z64 1 0 C00000


.PHONY: screenshot
screenshot:
	64drive_usb -d sm64_dump.z64 1 800000 70800