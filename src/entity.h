#ifndef ENTITY_H
#define ENTITY_H

#include "n64.h"

typedef struct entity_t /* Regular objects, Mario also has its own struct like this */
{
    u16    graph_node_type;        /* 0x00 */
    u16    graph_flags;
    struct entity_t* prev;                  /* previous linked list object */
    struct entity_t* next;                  /* next linked list object */ 
    u32    graph_parent;
    u32    graph_child;            /* 0x10 */
    u32    geo_layout_ptr;         /* 0x14 */
    u32    _18;
    u32    _1c;
    float  _20;             /* 0x20 */
    float  _24;
    float  _28;
    float  x_scaling;              /* 0x2c */
    float  y_scaling;              /* 0x30 */
    float  z_scaling;
    u16    _38;
    u16    _3a;
    u32    animation;              /* 0x3c - current animation */
    u16    anim_current_frame;     /* 0x40 */
    u16    anim_timer;             /* timer, animation related? */
    u16    anim_current_frame_copy;
    u16    _46;
    u32    _48;
    u32    _4c;
    u32    matrix_ptr;             /* 0x50 */
    float  float_54;
    float  float_58;
    float  float_5c;
    struct entity_t* next_object_ptr;        /* 0x60: re-check this */
    u32    _64;
    struct entity_t* next_object_ptr2;       /* 0x68: re-check this (child_obj) */
    u32    _6c;
    u32    _70;                  /* 0x70 */
    u16    active;                 /* 74 0x0000 = inactive, 0x0101 = active */
    u16    _76;                  /* collision flag according to YE */
    struct entity_t* collided_obj_ptr;      /* according to YE, pointer to object collided with */
    u32    _7c;
    u32    _80;                  /* 0x80 */
    u32    _84;
	u32    _88;
    u32    obj_flags;
    u32    _90;                  /* 0x90 */
    u32    _94;
    u32    _98;
    u32    _9c;
    //float  x_pos;                  /* 0xa0 */
    //float  y_pos;
    //float  z_pos;
    vec3_t position;
    //float  x_speed;  /* x increment? */
    //float  y_speed;                  /* 0xb0 */
    //float  z_speed;  /* z_increment? */
    vec3_t speed;
    float  speed_2;
    u32    _bc;
    u32    _c0;                  /* 0xc0 */
    u32    x_rotation;             /* 0xc4 - rotation triplet */
    u32    y_rotation;             /* 0xc8 */
    u32    z_rotation;
    u32    x_rotation2;            /* rotation copy (collision?) 0xd0 */
    u32    y_rotation2;            /* 0xd4 */
    u32    z_rotation2;
    u32    _dc;
    u32    _e0;                  /* 0xe0 */
    float  _e4;         /* gravity related? y_speed - 0xe4 ? */
    u32    _e8;
    u32    _ec;
    u32    _f0;                  /* 0xf0 */
    u32    _f4;               /* obj type for some behaviors (ie, ice bully), for AMPS, radius of rotation */
    u32    _f8;
    u32    _fc;
    u32    _100;                 /* 0x100 */
    u32    _104;
    u32    _108;
    u32    _10c;
    u32    _110;                 /* 0x110 */
    u32    _114;
    u32    _118;
    u32    _11c;
    u32    animation_ptr;        /* 0x120 = (set by 0x27 26 behavior command) entry for animation? */
    u32    _124;                  /* in some behaviors, action related? */
    float  _128;
    float  _12c;
    u32    interaction;            /* 0x130 
                                      00 = Something Solid. Can't grab. Mario walks around, Can jump over.
                                      01 = Crashed when jumping at it, Used by Hoot.
                                      02 = Grabbing
                                      04 = Going through door
                                      08 = Knocks mario back and dissappears. No damage.
                                      10 = Something Solid, Can't grab, Mario walks around, Can't jump over, Seems somewhat thin..
                                      40 = Climbing 
                                   */
    u32    _134;
    u32    _138;
    u32    _13c;
    u32    _140;                 /* 0x140 */
    u32    behav_param;            /* behav param */
    u32    _148;
    u32    action;
    u32    _150;                 /* 0x150 = also reset when action changes */
    u32    timer;                  /* always incremented. When action changes, it's set to 0 */
    float  _158;                 
    float  distance_from_mario;
    u32    _160;                 /* 0x160 */
    float  _164_x;
    float  _168_y;
    float  _16c_z;
    float  _170;                 /* 0x170 */
    float  _174;
    u32    _178;
    u32    transparency;
    u32    damage_to_mario;        /* According to YE, "How many segments of damage to do to Mario for objects that cause him harm" */
    u32    health;                 /* Health (ie, for King bob-omb and whomp */
    u32    behav_param2;           /* re-check */
    u32    previous_action;        /* used to reset the 0x154 timer */
    u32    _190;                 /* 0x190 */
    float  collision_distance;     /*  NOTE: if collision_distance < disappear_distance then disappear_distance = collision_distance */
    u32    _198;
    float  drawing_distance;
    u32    _1a0;                 /* 0x1a0 */
    u32    _1a4;
    u32    _1a8;
    u32    _1ac;
    u32    _1b0;                 /* 0x1b0 */
    u32    _1b4;
    u32    _1b8;
    u32    _1bc;
    u32    _1c0;                 /* 0x1c0 */
    u32    _1c4;
    u32    _1c8;
    u32    script_ptr;
    u32    stack_index;            /* 0x1d0 */
    u32    stack[8]; // will have to check on this size
    //u32    _1d8;
    //u32    _1dc;
    //u32    _1e0;                 /* 0x1e0 */
    //u32    _1e4;
    //u32    _1e8;
    //u32    _1ec;
    //u32    _1f0;                 /* 0x1f0 */
    u16    _1f4;
    u16    _1f6;
    float  col_sphere_x;
    float  col_sphere_y;
    float  _200;                 /* 0x200 */
    float  _204;
    float  _208;
    u32    behavior_script_entry;
    u32    _210;                 /* 0x210 */
    u32    collide_obj_ptr;        /* pointer to another object (collision happening)?. 
                                   Can be used to detect if Mario is on top of the object by comparing
                                   value with Mario's pointer */
    u32    collision_ptr;          /* set by behavior script (0x2A command) */
    u32    _21c;
    u32    _220;                 /* 0x220 */
    u32    _224;
    u32    _228;
    u32    _22c;
    u32    _230;                 /* 0x230 */
    u32    _234;
    u32    _238;
    u32    _23c;
    u32    _240;                 /* 0x240 */
    u32    _244;
    u32    _248;
    u32    _24c;
    u32    _250;                 /* 0x250 */
    u32    _254;
    u32    _258;
    u32    behav_param_copy_ptr;
} entity_t;

typedef struct
{
    int __private[0x22];
    union {
        int _int[0x76];
        float _float[0x76];
    } public;
} entity_raw_t;

void entity_copy_pos_rot(entity_t* target, entity_t* source);
void entity_copy_position(entity_t* target, entity_t* source);
void entity_copy_rotation(entity_t* target, entity_t* source);
void cur_entity_graph_x10(void);

#endif /* ENTITY_H */