#include "entity.h"


// 8029F0E0
void entity_copy_pos_rot(entity_t* target, entity_t* source)
{
	entity_copy_position(target, source);
	entity_copy_rotation(target, source);
}


// 8029F120
void entity_copy_position(entity_t* target, entity_t* source)
{
	target->x_pos = source->x_pos;
	target->y_pos = source->y_pos;
	target->z_pos = source->z_pos;
}

// 8029F148
void entity_copy_rotation(entity_t* target, entity_t* source)
{
    target->x_rotation = source->x_rotation;
    target->y_rotation = source->y_rotation;
    target->z_rotation = source->z_rotation;
	
    target->x_rotation2 = source->x_rotation2;
    target->y_rotation2 = source->y_rotation2;
    target->z_rotation2 = source->z_rotation2;
}


// 8029F6BC
void cur_entity_graph_x10(void)
{
	cur_entity->graph_flags |= 0x10;
}