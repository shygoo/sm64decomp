#ifndef HUD_H
#define HUD_H


#define HUD_LIVES  0x0001
#define HUD_COINS  0x0002
#define HUD_STARS  0x0004
#define HUD_CAMERA 0x0008
#define HUD_UNK_10 0x0010
#define HUD_UNK_20 0x0020
#define HUD_TIMER  0x0040
#define HUD_UNK_80 0x0080

#define HUD_POWER_CHANGING 0x8000

void hud_main(void);


#endif /* HUD_H */