#ifndef CI_H
#define CI_H

/*

	64drive Command Interface (CI)
	http://64drive.retroactive.be/64drive_hardware_spec.pdf

*/

// Interface addresses

#define CI_ADDR     0xB8000000
#define CI_ADDR_EXT 0xBF800000

// Interface structure

#pragma pack(push, 1)
typedef struct
{
	int   gpbuf[128]; // 000
	volatile unsigned short status;     // 200
	short _0;         // 202
	int   _1;         // 204
	int   command;    // 208
	int   _2;         // 20C
	int   lba;        // 210
	int   _3;         // 214
	int   length;     // 218
	
	char  _4[0xCC];   // 21c
	
	const int   sdram_size; // 2E8
	const int   hw_magic;   // 2EC
	const int   hw_variant; // 2F0
	
	int   pers_var_storage; // 2F4
	
	const short button;     // 2F8
	const short upgrade_module_status; // 2FA
	const short revision;   // 2FC
	
	char _5[0xD02];
	
	char eeprom[0x800];
	
	int save_wb_lba_list[0x100];
	
} ci_t;
#pragma pack(pop)

// Commands

#define CI_RD_SEC_INTO_BUF         0x01 // LBA
#define CI_RD_MULT_SECS_SDRAM      0x03 // LBA, RAMADDR, NUM_SEC

#define CI_WR_BUF_INTO_SEC         0x10 // LBA
#define CI_WR_MULT_SECS_SDRAM      0x13 // LBA, RAMADDR, NUM_SEC

#define CI_RESERVED_A              0x30
#define CI_RESERVED_B              0x31

#define CI_SET_SAVE_TYPE           0xD0 // SAVE_TYPE
#define CI_DISABLE_SAVE_WB         0xD1
#define CI_ENABLE_SAVE_WB          0xD2
#define CI_DISABLE_BSWAP_ON_LOAD   0xE0
#define CI_ENABLE_BSWAP_ON_LOAD    0xE1
#define CI_ENABLE_CARTROM_WR       0xF0
#define CI_DISABLE_CARTROM_WR      0xF1
#define CI_ENABLE_EXT_ADDR_MODE    0xF8
#define CI_DISABLE_EXT_ADDR_MODE   0xF9

#define CI_START_FW_UPGRADE        0xFA // MAGIC
#define CI_SET_CF_PULSE_WIDTH      0xFD // NUM_CYCLES

// Macros

#define ci_at(addr) ((ci_t*)addr)
#define ci_clear(ci) while((ci->status >> 12) == 1); // wait for status to clear
#define ci_command(ci, cmd) ci->command = cmd;

#define ci_enable_cartrom_writes(ci) \
	ci_clear(ci); \
	ci->command = CI_ENABLE_CARTROM_WR; \
	ci_clear(ci);
	
#define ci_disable_cartrom_writes(ci) \
	ci_clear(ci); \
	ci->command= CI_DISABLE_CARTROM_WR; \
	ci_clear(ci);
	
#define ci_button_pressed(ci) (ci->button == 0)
	
/*

ci_t* ci = ci_at(CI_ADDR);
ci_enable_cartrom_writes(ci);

g_cart_scratch_buf[0] = 1;

*/

#endif // CI_H

