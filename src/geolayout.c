///////////

typedef void (*geo_cmd_func_t)(void);

extern geo_cmd_func_t g_geo_cmd_funcs[32]; // 8038B810
extern u32 g_geo_stack[];        // 8038BCB8
extern u16 g_geo_stack_idx;      // 8038BD7A
extern u16 g_geo_stack_idx_copy; // 8038BD7E
extern u32 g_cur_geo_cmd[];      // 8038BD80

//////////

// 8037E0B4
void* parse_geo_layout(a0 = void* pvoid_something, a1 = u32 geo_segoffset)
{
	pvoid_8038BCA4 = 0;
	u32_8038BCB0 = 0;
	u32_8038BCF8 = 0;
	
	u16_8038BD78 = 0;
	
	g_geo_stack_idx = 2; // 8038BD7A stack index?
	g_geo_stack_idx_copy = 2; // 8038BD7E stack index copied here?
	
	g_cur_geo_cmd = SegmentedToVirtual(geo_segoffset)
	
	pvoid_8038BCA0 = pvoid_something;
	
	g_geo_stack[0] = 0; // 8038BCB8
	g_geo_stack[1] = 0;
	
	while(g_cur_geo_cmd != NULL)
	{
		g_geo_cmd_funcs[*(u8*)g_cur_geo_cmd]();
	}
	
	return pvoid_8038BCA4;
}

//////

// 8037CD60
void geo_00(void)
{
	// 01 00 00 00 [SS SS SS SS]
	//  jump to segment offset
	//  push vaddr of next command to the stack
	//  push some other thing to the stack
	
	g_geo_stack[g_geo_stack_idx++] = &g_cur_geo_cmd[2]; // push address of next command
	g_geo_stack[g_geo_stack_idx++] = (u16_8038BD78 << 16) + g_geo_stack_idx_copy;
	
	g_geo_stack_idx_copy = g_geo_stack_idx;
	
	g_cur_geo_command = SegmentedToVirtual(g_cur_geo_command[1]);
}

void geo_01(void)
{
	t6 = geo_stack_idx_copy
	g_geo_stack_idx = t6
	
	a0 = g_geo_stack_idx
	a0--
	
	t0 = 80390000
	at = 80390000
	
	t7 = a0 << 16
	
	a0 = t7
	t8 = t7 >> 16 // sign
	a0 = t8
	t8 = a0 * 4
	t0 = t0 + t8
	t0 = [t0 + bcb8]
	a0 = [at + bd7a]
	at = 80390000
	t1 = t0 & 0xFFFF
	[at + bd7e] = t1
	t2 = g_geo_stack_idx
	
	
	
}


/*

//80150E50 CG geo layout data
//8033B400 segment table

8038B810 geo layout dispatch table

00 8037CD60
01 8037CE24
02 8037CEE8
03 8037CF70
04 8037CFC0
05 8037D018
06 8037D050
07 8037D0D0
08 8037D1D0
09 8037D328
0A 8037D3A4
0B 8037D48C
0C 8037D500
0D 8037D55C
0E 8037D5D4
0F 8037D640
10 8037D6F0
11 8037D8D4
12 8037D998
13 8037DB74
14 8037DC10
15 8037DCD4
16 8037DD4C
17 8037DDDC
18 8037DE34
19 8037DE94
1A 8037DEF8
1B 8037DF1C
1C 8037DFD4
1D 8037DA5C
1E 8037DB50
1F 8037D4DC
20 8037E058

8037E0B4 asm read geo layout command and dispatch

8038BD80 geo layout current command

*/