#ifndef N64_H
#define N64_H

typedef unsigned int u32;
typedef unsigned short u16;
typedef unsigned char u8;

typedef int s32;
typedef short s16;
typedef char s8;

typedef struct
{
    float x;
    float y;
    float z;
} vec3_t;

typedef struct
{
    u32 dram_addr;
    u32 cart_addr;
    u32 rd_len;
    u32 wr_len;
    volatile u32 status;
    u32 bsd_dom1_lat;
    u32 bsd_dom1_pwd;
    u32 bsd_dom1_pgs;
    u32 bsd_dom1_rls;
    u32 bsd_dom2_lat;
    u32 bsd_dom2_pwd;
    u32 bsd_dom2_pgs;
    u32 bsd_dom2_rls;
} pi_t;

#define pi ((pi_t*)0xA4600000)

#define NULL ((void*)0)

#endif /* N64_H */