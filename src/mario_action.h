#define MARIO_ACTION_JUMP        0x03000880
#define MARIO_ACTION_DOUBLE_JUMP 0x03000881
#define MARIO_ACTION_TRIPLE_JUMP 0x01000882
#define MARIO_ACTION_BACKFLIP    0x01000883
#define MARIO_ACTION_SIDEFLIP    0x01000887
#define MARIO_ACTION_FALL        0x0100088C
#define MARIO_ACTION_LAND        0x04000470 // land from jump
#define MARIO_ACTION_LAND_2      0x04000471 // land from fall or jumpkick
#define MARIO_ACTION_LAND_3      0x04000472 // land from double jump
#define MARIO_ACTION_RESUME      0x0C000230
#define MARIO_ACTION_STAND       0x0C400201
#define MARIO_ACTION_LEDGE       0x0800034B
#define MARIO_ACTION_PUNCH       0x00800380 // or duck kick
#define MARIO_ACTION_PUNCH_RUN   0x00800457
#define MARIO_ACTION_DUCK        0x0C008220
#define MARIO_ACTION_DUCK_START  0x0C008221
#define MARIO_ACTION_DUCK_END    0x0C008222
#define MARIO_ACTION_SLIDE       0x00000050
#define MARIO_ACTION_RUN         0x04000440
#define MARIO_ACTION_RUN_END     0x0400044A
#define MARIO_ACTION_RUN_BRAKE   0x04000445
#define MARIO_ACTION_JUMP_FLY    0x03000894


#define MARIO_FLAGS_WING_CAP     0x00000008

int mario_is_collision_something(mario_t* mario);
int mario_action_yspd32(mario_t* mario, u32 action, u32 unknown);
int mario_set_action(mario_t* mario, u32 action, u32 unknown);
int mario_select_action_jump_running(mario_t* mario);
void mario_force_action(mario_t* mario, u32 action, u32 unknown);
void mario_set_action_effect_rotation_and_speed(mario_t* mario, u32 action, u32 unknown);
int mario_select_action_standing(mario_t* mario);