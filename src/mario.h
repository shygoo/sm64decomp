#ifndef MARIO_H
#define MARIO_H

#include "n64.h"
#include "collision.h"
#include "camera.h"
#include "input.h"
#include "entity.h"

typedef struct /* 8033b170 */
{
    //u32    status;
	u16    _00;
	u16    _02;
    u32    flags;                   /* cap & other flags */
    u32    _08;
    u32    action;                  /* see Romanian Girl list */
    u32    previous_action;         /* 0x10 */
    u32    _14;
    u16    _18;
    u16    _1a;
    u32    _1c;    
    float  _20;                   /* 0x20 */
    u16    _24;                    /* rotation related, if bit 1 of status is set, 0x24 is copied to 0x2e */
    s16    hitstun;                 /* hitstun counter (how long Mario stays invencible after getting hit */
    u32    _28;
	// _2b u8
    s16    _2c;
    u16    rotation;                /*2e divide it by 180 to get the angle? */
    s16    _30;                   /* 0x30 */
    s16    _32;    
    u32    _34;
    u32    _38;
    //float  x_pos;                   /* 0x3c */
    //float  y_pos;                   /* 0x40 */ 
    //float  z_pos; // 44
    vec3_t position;
    //float  x_speed; // 48
    //float  y_speed; // 4c
    //float  z_speed;                 /* 0x50. The next four floats are related to speed/acelleration */
    vec3_t speed;
    float  speed_2; // 54
    float  _58;
    float  _5c;    
    u32    _60;                   /* 0x60 */
    u32    _64;
    collision_face_t *cur_collision_face;  /* 68 current triangle mario is stepping in */
    float  _6c;    
    float  ground_y;                   /* 0x70 - ground Y */
    //u32    _74; 
	u16 _74; // rotation related
	u16 _76;
    u32    _78;
    u32    _7c;    
    u32    _80;                   /* 0x80 */
    u32    _84;
    entity_t* entity;
    u32    _8c_ptr;    
    u32    Mario_level_command;     /* 0x90 = 8033b4b0 = Information read from the Level command that sets Mario*/
    camera_t* camera;
    u32    _98_ptr;               /* 0x8033B3B0 */
    ctrl_main_t* pad;                    /* pointer to controller struct  controller 1 = 8033AF90   controller2 = 8033AFAC*/
    //mario_animation_t* MarioAnimationStruct;   /* 0x8033B080 */
    void* mario_animation;
    u32    _a4;
    s16    coins;                   /* 0xa8 */
    s16    stars;                   /* 0xaa */
    u8     keys;                    /* 0xac (fixed) */
	u8     lives;                   /* 0xad */
    s16    power;                   /* 0xae */
    u16    constant_ground_distance;   /* usually 0xBD */
	u8     time_power_drain; // 0xB2
	u8     time_power_heal; // 0xB3
    u16    misc_timer;              /* on any value other than zero it will decrease until zero (also, drains mario energy?) */
    u32    cap_timer; // b4 is u8?
    u32    _b8;
    float  _bc;    
    float  _c0;                   /* 0xc0 */
} mario_t;

#endif /* MARIO_H */