#ifndef INPUT_H
#define INPUT_H

#define DEMO_BTN_C_RT  0x01
#define DEMO_BTN_C_LT  0x02
#define DEMO_BTN_C_DN  0x04
#define DEMO_BTN_C_UP  0x08
#define DEMO_BTN_START 0x10 // not used? causes termination
#define DEMO_BTN_Z     0x20
#define DEMO_BTN_B     0x40
#define DEMO_BTN_A     0x80

typedef struct
{
	u8 num_frames; // terminate when 1
	u8 analog_x; // top bit = up/down, rest = pressure (not signed)
	u8 analog_y;
	u8 buttons;
} demo_command_t; // 80064718 to 80064C04

typedef struct // 4 bytes
{ 
	u16 _00;
	u8  _02;
	u8  _03;
} ctrl_thing_a_t; // related function 80323F04 

typedef struct
{
	u16 buttons;  // 00
	s8  analog_x; // 02
	s8  analog_y; // 03
	u8  unk;      // 04
	unsigned : 8; // 05 padding probably
} ctrl_thing_b_t;

typedef struct // 28 bytes
{
	s16             analog_x;      // 00
	s16             analog_y;      // 02
	float           analog_x_fp;   // 04
	float           analog_y_fp;   // 08
	float           analog_abs_fp; // 0C positive value, both axes
	u16             buttons;       // 10
	u16             buttons_temp;  // 12 ?
	ctrl_thing_a_t* ptr_a;         // 14 // 8033AFE8
	ctrl_thing_b_t* ptr_b;         // 18 // 8033AFF8
} ctrl_thing_t;

typedef struct
{
	ctrl_thing_t c0; // 00 8033AF90
	ctrl_thing_t c1; // 1C 8033AFAC demo cmd parser sets everything to 0?
	ctrl_thing_t c2; // 38 8033AFC8 analog & buttons mirror c0
	unsigned : 32; // 8033AFE4 ?? never touched
} ctrl_main_t;

#endif // INPUT_H
