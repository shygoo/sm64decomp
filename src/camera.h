#ifndef CAMERA_H
#define CAMERA_H

typedef struct camera_struct  /* mario->camera (0x8033C520) */
{
    u32 mario_action;       /* copied from mario->action */
    float x;               /* also copied from Mario struct */ 
    float y;
    float z;
    s16 _0x10_mario_0x2c;  /* 0x10 */
    u16 rotation;          /* again copied from Mario struct */
    s16 _0x14_mario_0x30;
    s16 _0x16_mario_0x32;
    u32 _0x18;
    u16 _0x1c;
    u16 camera_setting;    /* 0x06 = door opening   0x09 = triggers initial peach animation */
    
     /* incomplete, many other members left ?? */ 
} camera_t;

#endif /* CAMERA_H */