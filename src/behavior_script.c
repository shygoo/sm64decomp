#include "global.h"
#include "entity.h"
#include "n64.h"

// todo maybe union everything 0x88+ in the entity struct with arrays to clean up

// 80383E5C
int bhv_22(void)
{
	g_cur_entity_graph_x10(); // g_cur_entity->graph_flags |= 0x10;
	g_bhv_cmd_ptr++;
	return 0;
}

// 80383EA0
int bhv_35(void)
{
	g_cur_entity->graph_flags &= 0xFFFE; // ~1
	g_bhv_cmd_ptr++;
	return 0;
}

// 80383EE4
int bhv_21(void)
{
	g_cur_entity->graph_flags |= 4;
	g_bhv_cmd_ptr++;
	return 0;
}

// 80383F24
int bhv_1b(void)
{
	g_cur_entity->geo_layout_ptr = g_geo_thing_ptr[g_bhv_cmd_ptr[0] & 0xFFFF];
	g_bhv_cmd_ptr++;
	return 0;
}

// 80383F94
int bhv_1c(void)
{
	u32 arg0 = g_bhv_cmd_ptr[1];
	u32 arg1 = g_bhv_cmd_ptr[2];
	
	u32 res = _8029ED20(g_cur_entity, 0, arg0, arg1);
	
	entity_copy_pos_rot(res, g_cur_entity);
	
	g_bhv_cmd_ptr += 3;
	return 0;
}


// 8038401C
int bhv_2c(void)
{
	u32 arg0 = g_bhv_cmd_ptr[1];
	u32 arg1 = g_bhv_cmd_ptr[2];
	
	entity_t* ent = _8029ED20(g_cur_entity, 0, arg0, arg1);
	
	entity_copy_pos_rot(ent, g_cur_entity);
	
	g_cur_entity->_6c = ent;
	
	g_bhv_cmd_ptr += 3;
	return 0;
}

// 803840B4
int bhv_29(void)
{
	u32 p0 = g_bhv_cmd_ptr[0] & 0xFFFF; // signed?
	u32 p1 = g_bhv_cmd_ptr[1];
	u32 p2 = g_bhv_cmd_ptr[2];
	
	entity_t* ent = _8029ED20(g_cur_entity, 0, p1, p2);
	
	entity_copy_pos_rot(ent, g_cur_entity);
	
	ent->behav_param = p0;
	
	g_bhv_cmd_ptr += 3;
	return 0;
}
// 80384164
int bhv_1d(void)
{
	g_cur_entity->active = 0;
	return 1;
}

// 80384188
int bhv_0a(void)
{
	return 1;
}

// 803841A0
int bhv_0b(void)
{
	return 1;
}

// 803841B8
int bhv_02(void)
{
	g_bhv_cmd_ptr++;
	g_cur_entity_stack_push(&g_bhv_cmd_ptr[1]);
	g_bhv_cmd_ptr = SegmentedToVirtual(g_bhv_cmd_ptr[0]);
	return 0;
}

// 80384224
int bhv_03(void)
{
	g_bhv_cmd_ptr = g_cur_entity_stack_pop();
	return 0;
}

// 8038425C
int bhv_01(void)
{
	if(g_cur_entity->_1f4 < (g_bhv_cmd_ptr[0] & 0xFFFF) - 1)
	{
		g_cur_entity->_1f4++;
	}
	else
	{
		g_cur_entity->_1f4 = 0;
		g_bhv_cmd_ptr++;
	}
	return 1;
}

// 803842E4
int bhv_25(void)
{
	u8 ent_idx = (g_bhv_cmd_ptr[0] >> 16) & 0xFF; //byte
	
	//int sp_0 = *(u32*)(g_cur_entity + (ent_idx * 4) + 0x88);
	int sp_0 = g_cur_entity->public_int[ent_idx];

	if(g_cur_entity->_1f4 < (sp_0 - 1))
	{
		g_cur_entity->_1f4++;
		// no cmd pointer update?
	}
	else
	{
		g_cur_entity->_1f4 = 0;
		g_bhv_cmd_ptr++;
	}
	return 1;
}

// 8038438C
int bhv_04(void)
{
	g_bhv_cmd_ptr++;
	g_bhv_cmd_ptr = SegmentedToVirtual(g_bhv_cmd_ptr[0]);
	return 0;
}

// 803843E0
int bhv_26(void)
{
	int param = (g_bhv_cmd_ptr[0] >> 16) & 0xFF;
	
	g_cur_entity_stack_push(&g_bhv_cmd_ptr[1]);
	g_cur_entity_stack_push(param);
	
	g_bhv_cmd_ptr++;
	
	return 0;
}

// 80384450
int bhv_05(void)
{
	short param = g_bhv_cmd_ptr[0] & 0xFFFF;
	
	g_cur_entity_stack_push(&g_bhv_cmd_ptr[1]);
	g_cur_entity_stack_push(param);
	
	g_bhv_cmd_ptr++;
	
	return 0;
}

// 803844C0
int bhv_06(void)
{
	void* sp_1c = g_cur_entity_stack_pop() - 1;
	
	if(sp_1c != 0)
	{
		g_bhv_cmd_ptr = g_cur_entity_stack_pop();
		g_cur_entity_stack_push(g_bhv_cmd_ptr);
		g_cur_entity_stack_push(sp_1c);
	}
	else
	{
		g_cur_entity_stack_pop();
		g_bhv_cmd_ptr++;
	}
	return 1;
}

// 80384554
int bhv_07(void)
{
	// same as 06, but returns 1
	
	void* sp_1c = g_cur_entity_stack_pop() - 1;
	
	if(sp_1c != NULL)
	{
		g_bhv_cmd_ptr = g_cur_entity_stack_pop();
		g_cur_entity_stack_push(g_bhv_cmd_ptr);
		g_cur_entity_stack_push(sp_1c);
	}
	else
	{
		g_cur_entity_stack_pop();
		g_bhv_cmd_ptr++;
	}
	return 0;
}

// 803845E8
int bhv_08(void)
{
	g_cur_entity_stack_push(&g_bhv_cmd_ptr[1]);
	g_bhv_cmd_ptr++;
	return 0;
}

// 80384634
int bhv_09(void)
{
	g_bhv_cmd_ptr = g_cur_entity_stack_pop();
	g_cur_entity_stack_push(g_bhv_cmd_ptr);
	return 1;
}

// 80384678
int bhv_0c(void)
{
	void (*func)(void);
	
	func = (void*) g_bhv_cmd_ptr[1];
	
	func();
	
	g_bhv_cmd_ptr += 2;
	return 0;
}

// 803846D0
int bhv_0e(void)
{
	u8 ent_idx = (g_bhv_cmd_ptr[0] >> 16) & 0xFF;
	short arg_val = g_bhv_cmd_ptr[0] & 0xFFFF;
	
	//*(float*)(&g_cur_entity->_88 + (ent_idx * 4)) = (float) f4;
	g_cur_entity->public_float[ent_idx] = (float)arg_val;
	
	g_bhv_cmd_ptr++;
	return 0;
}

// 8038475C
int bhv_10(void)
{
	u8 ent_idx = (g_bhv_cmd_ptr[0] >> 16) & 0xFF;
	short arg_val = g_bhv_cmd_ptr[0] & 0xFFFF;
	
	//*(int*)(&g_cur_entity->_88 + (ent_idx * 4)) = arg_val;
	g_cur_entity->public_int[ent_idx] = arg_val;
	
	g_bhv_cmd_ptr++;
	return 0;
}

// 803847D4
int bhv_36(void)
{
	// same as 10 but layout is different?
	// this one also explicitly signs the short with sll/sra
	
	u8 ent_idx = (g_bhv_cmd_ptr[0] >> 16) & 0xFF;
	short arg_val = g_bhv_cmd_ptr[1] & 0xFFFF;
	
	//*(int*)(&g_cur_entity->_88 + (ent_idx * 4)) = arg_val;
	g_cur_entity_raw->public._int[ent_idx] = arg_val;

	g_bhv_cmd_ptr += 2;
	return 0;
}

// 80384854
int bhv_14(void)
{
	// 14[II][AAAA] [BBBB][....]
	
	u8 ent_idx = (g_bhv_cmd_ptr[0] >> 16) & 0xFF;
	float sp_20 = (short)(g_bhv_cmd_ptr[0] & 0xFFFF);
	float sp_1c = (short)(g_bhv_cmd_ptr[1] >> 16);
	
	//*(float*)(&g_cur_entity->_88 + (ent_idx * 4)) = (_80383CB4() * sp_1c) + sp_20;
	g_cur_entity_raw->public._float[ent_idx] = (_80383CB4() * sp_1c) + sp_20;

	g_bhv_cmd_ptr += 2;
	return 0;
}

// 80384928
int bhv_15(void)
{
	// same as 14, but casts result to int
	
	u8 ent_idx = (g_bhv_cmd_ptr[0] >> 16) & 0xFF;
	int sp_20 = (short)(g_bhv_cmd_ptr[0] & 0xFFFF);
	int sp_1c = (short)(g_bhv_cmd_ptr[1] >> 16);
	
	//*(int*)(&g_cur_entity->_88 + (ent_idx * 4)) = (int)(_80383CB4() * (float)sp_1c) + sp_20;
	g_cur_entity_raw->public._int[ent_idx] = (int)(_80383CB4() * (float)sp_1c) + sp_20;
	
	g_bhv_cmd_ptr += 2;
	return 0;
}

// 803849F8
int bhv_13(void)
{
	u8 ent_idx = (g_bhv_cmd_ptr[0] >> 16) & 0xFF;
	int sp_20 = (short)(g_bhv_cmd_ptr[0] & 0xFFFF);
	int sp_1c = (short)(g_bhv_cmd_ptr[1] >> 16);
	
	//*(int*)(&g_cur_entity->_88 + (ent_idx * 4)) = (_80383BB0() >> sp_1c) + sp_20;
	g_cur_entity_raw->public._int[ent_idx] = (_80383BB0() >> sp_1c) + sp_20;
	
	g_bhv_cmd_ptr += 2;
	return 0;
}

// 80384AB4
int bhv_16(void)
{
	u8 ent_idx = (g_bhv_cmd_ptr[0] >> 16) & 0xFF;
	float sp_20 = (short)(g_bhv_cmd_ptr[0] & 0xFFFF);
	float sp_1c = (short)(g_bhv_cmd_ptr[1] >> 16);
	
	//*(float*)(&g_cur_entity->_88 + (ent_idx * 4)) += (_80383CB4() * sp_1c) + sp_20;
	g_cur_entity_raw->public._float[ent_idx] += (_80383CB4() * sp_1c) + sp_20;
	
	g_bhv_cmd_ptr += 2;
}

// 80384B90
int bhv_17(void)
{
	// like 13 but addition
	u8 ent_idx = (g_bhv_cmd_ptr[0] >> 16) & 0xFF;
	int sp_20 = (short)(g_bhv_cmd_ptr[0] & 0xFFFF);
	int sp_1c = (short)(g_bhv_cmd_ptr[1] >> 16);
	
	//*(int*)(&g_cur_entity->_88 + (ent_idx * 4)) += (_80383BB0() >> sp_1c) + sp_20;
	g_cur_entity_raw->public._int[ent_idx] += (_80383BB0() >> sp_1c) + sp_20;
	
	g_bhv_cmd_ptr += 2;
	return 0;
}

// 80384C5C
int bhv_0d(void)
{
	u8 ent_idx = (g_bhv_cmd_ptr[0] >> 16) & 0xFF; // sp_07
	float value = (short)(g_bhv_cmd_ptr[0] & 0xFFFF); // sp_00
	
	//*(float*)(&g_cur_entity->_88 + (ent_idx * 4)) += value;
	g_cur_entity_raw->public._float[ent_idx] += value;
	
	g_bhv_cmd_ptr++;
	return 0;
}

// 80384CF0
int bhv_0f(void)
{
	u8 ent_idx = (g_bhv_cmd_ptr[0] >> 16) & 0xFF;
	short value = g_bhv_cmd_ptr[0] & 0xFFFF;
	
	//*(int*)(&g_cur_entity->_88 + (ent_idx * 4)) += value;
	g_cur_entity_raw->public._int[ent_idx] += value;

	g_bhv_cmd_ptr++;
	return 0;
}

// 80384D70
int bhv_11(void)
{
	// set flags
	
	u8 ent_idx = (g_bhv_cmd_ptr[0] >> 16) & 0xFF;
	int value = (short)(g_bhv_cmd_ptr[0] & 0xFFFF);
	
	value &= 0xFFFF; // ?
	
	//*(int*)(&g_cur_entity->_88 + (ent_idx * 4)) |= value;
	g_cur_entity_raw->public._int[ent_idx] |= value;
	
	g_bhv_cmd_ptr++;
	return 0;
}

// 80384E04
int bhv_12(void)
{
	// unset flags
	
	u8 ent_idx = (g_bhv_cmd_ptr[0] >> 16) & 0xFF;
	int value = (short)(g_bhv_cmd_ptr[0] & 0xFFFF);
	
	value = (value & 0xFFFF) ^ 0xFFFF;
	
	//*(int*)(&g_cur_entity->_88 + (ent_idx * 4)) &= value;
	g_cur_entity_raw->public._int[ent_idx] &= value;

	g_bhv_cmd_ptr++;
	return 0;
}

// 80384E9C
int bhv_27(void)
{
	u8 ent_idx = (g_bhv_cmd_ptr[0] >> 16) & 0xFF;
	
	//*(int*)(&g_cur_entity->_88 + (ent_idx * 4)) = g_bhv_cmd_ptr[1];
	g_cur_entity_raw->public._int[ent_idx] = g_bhv_cmd_ptr[1];

	g_bhv_cmd_ptr += 2;
	return 0;
}

// 80384F08
int bhv_28(void)
{
	// set animation index?
	
	int anim_idx = (g_bhv_cmd_ptr[0] >> 16) & 0xFF;
	
	u32* anim_ptr = g_cur_entity->animation_ptr;
	
	_8037C658(g_cur_entity, anim_ptr[anim_idx]);
	
	g_bhv_cmd_ptr++;
	return 0;
}

// 80384F8C
int bhv_1e(void)
{
	
}

// 8038503C
int bhv_18(void)
{
	// incomplete
	u8 ent_idx = (g_bhv_cmd_ptr[0] >> 16) & 0xFF;
	g_bhv_cmd_ptr++;
	return 0;
}

// 80385084
int bhv_1A(void)
{
	// incomplete
	u8 ent_idx = (g_bhv_cmd_ptr[0] >> 16) & 0xFF;
	g_bhv_cmd_ptr++;
	return 0;
}

// 803850CC
int bhv_19(void)
{
	// incomplete
	u8 ent_idx = (g_bhv_cmd_ptr[0] >> 16) & 0xFF;
	g_bhv_cmd_ptr++;
	return 0;
}

/*****************************************/
/*****************************************/

// 80383DBC
void g_cur_entity_stack_push(int value)
{
	g_cur_entity->stack[g_cur_entity->stack_index] = value;
	g_cur_entity->stack_index++;
}

// 80383DF8
u32 g_cur_entity_stack_pop(void)
{
	g_cur_entity->stack_index--;
	return g_cur_entity->stack[g_cur_entity->stack_index];
}

/*
// 80383CB4
void _80383CB4()
{
	int v0 = _80383BB0();
	
	float f = (float) v0; 
	
	if(v0 < 0)
	{
		f += (float)0xFFFFFFFF; // 0x4F800000
	}
	
	double f4 = f16 / f18
	
	(double)7.5
	
}
*/

/*

Command handlers (7572 bytes)

  80383E5C 0x22 x
  80383EA0 0x35 x
  80383EE4 0x21 x
  80383F24 0x1B x
  80383F94 0x1C x
  8038401C 0x2C x
  803840B4 0x29 x
  80384164 0x1D x
  80384188 0x0A x
  803841A0 0x0B x
  803841B8 0x02 x
  80384224 0x03 x
  8038425C 0x01 x
  803842E4 0x25 x
  8038438C 0x04 x
  803843E0 0x26 x
  80384450 0x05 x
  803844C0 0x06 x
  80384554 0x07 x
  803845E8 0x08 x
  80384634 0x09 x
  80384678 0x0C x
  803846D0 0x0E x
  8038475C 0x10 x
  803847D4 0x36 x
  80384854 0x14 x
  80384928 0x15 x
  803849F8 0x13 x
  80384AB4 0x16 x
  80384B90 0x17 x
  80384C5C 0x0D x
  80384CF0 0x0F x
  80384D70 0x11 x
  80384E04 0x12 x
  80384E9C 0x27 x
  80384F08 0x28 x
  80384F8C 0x1E 
  8038503C 0x18 x
  80385084 0x1A x
  803850CC 0x19 x
  80385114 0x1F 
  803851D0 0x20 
  8038528C 0x23 
  8038531C 0x2E 
  803853AC 0x2B 
  8038546C 0x24 
  803854CC 0x00 
  803856A0 0x2A 
  80385700 0x2D 
  8038575C 0x2F 
  803857A0 0x31 
  803857E4 0x32 
  8038586C 0x30 
  80385A60 0x33 
  80385AF0 0x37
  80385B4C 0x34

Script processor

  80385BE8 

Dependencies

  80277F50 // SegmentedToVirtual

  8029ED20
  8029F6BC   x g_cur_entity_graph_x10
  8029F0E0   x entity_copy_pos_rot
    8029F120 x entity_copy_position
    8029F148 x entity_copy_rotation
  
  80383BB0
  80383CB4
  80383DBC x g_cur_entity_stack_push
  80383DF8 x g_cur_entity_stack_pop
  
*/