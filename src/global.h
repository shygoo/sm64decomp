#ifndef GLOBAL_H
#define GLOBAL_H

#include "n64.h"
#include "mario.h"
#include "input.h"
#include "entity.h"

extern u32 g_num_frames; // 8032D5D4 global frame count

extern demo_command_t* g_cur_demo_cmd; // 8032D5F0 points to input command list for press start screen

extern mario_t* g_mario; // 8032D93C, always 0x8033B170

extern void* g_geo_thing_ptr[]; // 8032DDC4 (see 80383F24 behv 0x1B)

extern int   g_hud_updates_disabled; // 8032DDD0

extern float g_unk_float0; // 803365C4; // 3F7746EA 0.9659258127212524
extern float g_unk_float1; // 803365C8; // 3F708FB2 0.9396926164627075
extern float g_unk_float2; // 803365CC; // 3F5DB3D7 0.8660253882408142
extern float g_unk_float3; // 803365D0; // 3F5DB3D7 0.8660253882408142

extern ctrl_main_t     g_ctrl_main; // 8033AF90
extern ctrl_thing_a_t  g_ot0;  // 8033AFE8
extern ctrl_thing_a_t  g_ot1;  // 8033AFEC
extern ctrl_thing_a_t  g_ot2;  // 8033AFF0
extern ctrl_thing_a_t  g_ot3;  // 8033AFF4
extern ctrl_thing_b_t  g_aot0; // 8033AFF8 // demo doesn't touch these
extern ctrl_thing_b_t  g_aot1; // 8033AFFE
extern ctrl_thing_b_t  g_aot2; // 8033B004
extern ctrl_thing_b_t  g_aot3; // 8033B00A

extern u16  g_hud_num_lives;   // 8033B260
extern u16  g_hud_num_coins;   // 8033B262
extern u16  g_hud_num_stars;   // 8033B264
extern u16  g_hud_power;       // 8033B266
extern u16  g_hud_num_keys;    // 8033B268
extern u16  g_hud_state;       // 8033B26A

extern s16  g_hud_coins_enabled; // 8033BAC6

extern entity_t* g_cur_entity; // 80361160
extern entity_raw_t* g_cur_entity_raw; // 80361160 for behavior scripts

extern int* g_bhv_cmd_ptr; // 80361164

//const bhv_func_t g_behavior_script_table[]; // 8038B9B0

extern void PlaySound(u32 sound, vec3_t* position);

#endif /* GLOBAL_H */