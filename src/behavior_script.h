#ifndef BEHAVIOR_SCRIPT_H
#define BEHAVIOR_SCRIPT_H

typedef int (*bhv_func_t)(void);

int bhv_22(void);
int bhv_35(void);
int bhv_21(void);
int bhv_1b(void);
int bhv_1c(void);
int bhv_2c(void);
int bhv_29(void);
int bhv_1d(void);
int bhv_0a(void);
int bhv_0b(void);
int bhv_02(void);
int bhv_03(void);
int bhv_01(void);
int bhv_25(void);
int bhv_04(void);
int bhv_26(void);
int bhv_05(void);
int bhv_06(void);
int bhv_07(void);
int bhv_08(void);
int bhv_09(void);
int bhv_0c(void);
int bhv_0e(void);
int bhv_10(void);
int bhv_36(void);
int bhv_14(void);
int bhv_15(void);
int bhv_13(void);
int bhv_16(void);
int bhv_17(void);
int bhv_0d(void);
int bhv_0f(void);
int bhv_11(void);
int bhv_12(void);
int bhv_27(void);
int bhv_28(void);
int bhv_1e(void);
int bhv_18(void);
int bhv_1A(void);
int bhv_19(void);

#endif /* BEHAVIOR_SCRIPT_H */