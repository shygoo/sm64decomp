.definelabel PlaySound, 0x8031EB00

.definelabel g_num_frames, 0x8032D5D4 //global frame count

.definelabel g_mario, 0x8032D93C

.definelabel g_hud_updates_disabled, 0x8032DDD0

.definelabel g_hud_num_lives, 0x8033B260
.definelabel g_hud_num_coins, 0x8033B262
.definelabel g_hud_num_stars, 0x8033B264
.definelabel g_hud_power,     0x8033B266
.definelabel g_hud_num_keys,  0x8033B268
.definelabel g_hud_state,     0x8033B26A

.definelabel g_hud_coins_enabled, 0x8033BAC6